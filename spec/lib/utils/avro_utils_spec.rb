require 'spec_helper'


=begin
  String: :string,
  Symbol: :string,
  Fixnum: :int,
  Bignum: :long,
  Float: :double,
  TrueClass: :boolean,
  FalseClass: :boolean,
  NilClass: :null  
=end

SIMPLE_HASH_DATA = {
  string_name: 'Simple Json',
  symbol_name: :simple_json,
  age: 2,
  weight: 1000000000000000000000000000000000000000,
  distance: 10.5,
  valid: true,
  deleted: false,
  owner: nil
}

SIMPLE_SCHEMA = <<-JSON
{ "type": "record",
  "name": "SimpleJson",
  "fields" : [
    {"name": "string_name", "type": "string"},
    {"name": "symbol_name", "type": "string"},
    {"name": "age", "type": "int"},
    {"name": "weight", "type": "long"},
    {"name": "distance", "type": "double"},
    {"name": "valid", "type": "boolean"},
    {"name": "deleted", "type": "boolean"},
    {"name": "owner", "type": "null"}
  ]}
JSON


COMPLEX_HASH_DATA = {
  string_name: 'Simple Json',
  symbol_name: :simple_json,
  phone_numbers: ['405-123-4567', '405-456-0978'],
  address: {
    street: '1020 Mc Carthy Blvd',
    city: 'Milpitas',
    state: 'CA',
    zip: 95035
  }
}

COMPLEX_HASH_DATA_2 = {
  string_name: 'Simple Json 2',
  symbol_name: :simple_json_2,
  phone_numbers: ['504-123-4567', '504-456-0978'],
  address: {
    street: '260 East Tasman Drive',
    city: 'San Jose',
    state: 'CA',
    zip: 95134
  }
}

COMPLEX_SCHEMA = <<-JSON
{ "type": "record",
  "name": "ComplexJson",
  "fields" : [
    {"name": "string_name", "type": "string"},
    {"name": "symbol_name", "type": "string"},
    {"name": "phone_numbers", "type": { "type": "array", "items": "string" } },
    {"name": "address", "type": {
                          "type": "record",
                          "name": "Address",
                          "fields": [
                            {"name": "street", "type": "string"},
                            {"name": "city", "type": "string"},
                            {"name": "state", "type": "string"},
                            {"name": "zip", "type": "int"}
                          ]
                        }
    }
  ]}
JSON

TEST_FILE_PATH = File.expand_path('./tmp')
TEST_FILE_NAME = 'data.avr'
FULL_FILE_NAME = "#{TEST_FILE_PATH}/#{TEST_FILE_NAME}"


describe AvroUtils do
  before :all do
    FileUtils.mkdir_p(TEST_FILE_PATH) unless Dir.exists?(TEST_FILE_PATH) 
  end

  after :each do
    FileUtils.rm_rf(FULL_FILE_NAME)
  end

  context 'avro_schema_hash' do
    it 'generates avro schema for valid non-nested json' do
      expect(AvroUtils.avro_schema_hash(SIMPLE_HASH_DATA, 'SimpleJson').to_json).to eql(JSON.parse(SIMPLE_SCHEMA).to_json)
    end

    it 'generates avro schema for valid nested json' do
      expect(AvroUtils.avro_schema_hash(COMPLEX_HASH_DATA, 'ComplexJson').to_json).to eql(JSON.parse(COMPLEX_SCHEMA).to_json)
    end

    it 'throws InvalidDataException if the first argument is not a hash' do
      expect { AvroUtils.avro_schema_hash('invalid json', 'InvalidJson') }.to raise_error(InvalidDataException)
    end
  end

  context 'avro_schema' do
    it 'creates correct avro schema object from avro schema hash' do
      expected_schema = Avro::Schema.parse(COMPLEX_SCHEMA)
      result_schema   = AvroUtils.avro_schema(COMPLEX_HASH_DATA, 'ComplexJson')


      expect(result_schema.class).to eql(Avro::Schema::RecordSchema)

      [:type_sym, :name, :namespace, :fullname, :fields].each do |attr|
        expect(result_schema.send(attr).to_json).to eql(expected_schema.send(attr).to_json)
      end
    end

    it 'throws InvalidDataException if the first argument is not a hash' do
       expect { AvroUtils.avro_schema('invalid json', 'InvalidJson') }.to raise_error(InvalidDataException)
    end
  end

  context 'json_to_avro' do
    it 'creates correct avro data if the first argument is a valid json' do
      avro_data = AvroUtils.json_to_avro(COMPLEX_HASH_DATA.to_json, 'ComplexJson')

      datum_reader = Avro::IO::DatumReader.new
      read_data = Avro::DataFile::Reader.new(StringIO.new(avro_data.read), datum_reader)
      expect(read_data.first.to_json).to eql(COMPLEX_HASH_DATA.to_json)
      avro_data.rewind
    end

    it 'creates correct avro data if the first argument is a hash' do
      avro_data = AvroUtils.json_to_avro(COMPLEX_HASH_DATA, 'ComplexJson')

      datum_reader = Avro::IO::DatumReader.new
      read_data = Avro::DataFile::Reader.new(StringIO.new(avro_data.read), datum_reader)
      expect(read_data.first.to_json).to eql(COMPLEX_HASH_DATA.to_json)
      avro_data.rewind
    end

    it 'writes avro data to a file if a filename is passed' do
      avro_data = AvroUtils.json_to_avro(COMPLEX_HASH_DATA, 'ComplexJson', FULL_FILE_NAME)

      expect(File.exists?(FULL_FILE_NAME)).to be true
      expect(avro_data.is_a? File).to be true

      avro_data.reopen(FULL_FILE_NAME, 'rb')
      reader = Avro::DataFile::Reader.new(avro_data, Avro::IO::DatumReader.new)
      reader.each do |record|
        expect(record.to_json).to eql(COMPLEX_HASH_DATA.to_json)
      end
    end

    it 'throws InvalidDataException if the first argument is not a hash or a valid json' do
       expect { AvroUtils.json_to_avro('invalid json', 'InvalidJson') }.to raise_error(InvalidDataException)
    end

  end

  context 'bulk_json_to_avro' do
    it 'creates correct avro data if the first argument is a collection' do
      avro_data = AvroUtils.bulk_json_to_avro([COMPLEX_HASH_DATA, COMPLEX_HASH_DATA_2], 'ComplexJson')

      datum_reader = Avro::IO::DatumReader.new
      read_data = Avro::DataFile::Reader.new(StringIO.new(avro_data.read), datum_reader)
      expect(read_data.first.to_json).to eql(COMPLEX_HASH_DATA.to_json)
      expect(read_data.first.to_json).to eql(COMPLEX_HASH_DATA_2.to_json)
      avro_data.rewind
    end

    it 'writes avro data to a file if a filename is passed' do
      collection = [COMPLEX_HASH_DATA, COMPLEX_HASH_DATA_2]
      avro_data = AvroUtils.bulk_json_to_avro(collection, 'ComplexJson', FULL_FILE_NAME)

      expect(File.exists?(FULL_FILE_NAME)).to be true
      expect(avro_data.is_a? File).to be true

      avro_data.reopen(FULL_FILE_NAME, 'rb')
      reader = Avro::DataFile::Reader.new(avro_data, Avro::IO::DatumReader.new)
      reader.each_with_index do |record, idx|
        expect(record.to_json).to eql(collection[idx].to_json)
      end

    end

    it 'throws InvalidDataException if the first argument is not a hash or a valid json' do
       expect { AvroUtils.bulk_json_to_avro('invalid json', 'InvalidJson') }.to raise_error(InvalidDataException)
    end

  end

  context 'hash_with_string_keys' do
    it 'converts a valid json to hash with string keys' do
      expect(AvroUtils.hash_with_string_keys('{ "name": "Device" }')).to eql({ 'name' => 'Device'})
    end

    it 'converts a hash with symbol keys to hash with string keys' do
      expect(AvroUtils.hash_with_string_keys({ name: 'Device' })).to eql({ 'name' => 'Device'})
    end

    it 'returns a hash with string keys' do
      expect(AvroUtils.hash_with_string_keys({ 'name' => 'Device' })).to eql({ 'name' => 'Device'})
    end

    it 'throws InvalidDataException if the argument is not a hash or a valid json' do
       expect { AvroUtils.hash_with_string_keys('invalid json') }.to raise_error(InvalidDataException)
    end

  end

  context 'is_valid_schema?' do
    before :all do
      [ :invalid_character_in_name,  :invalid_undefined_type, :referenced_schemas, 
        :invalid_duplicate_type, :nested_schema ].each do |filename|
        eval("@#{filename} = #{File.read("./spec/fixtures/#{filename}.avsc")}")
      end
    end

    it 'validates avro schema in string format' do
      expect(AvroUtils.is_valid_schema?(@nested_schema.to_json)).to be true
      expect(AvroUtils.is_valid_schema?(@referenced_schemas.to_json)).to be true
    end

    it 'validates avro schema in hash format' do
      expect(AvroUtils.is_valid_schema?(@nested_schema)).to be true
      expect(AvroUtils.is_valid_schema?(@referenced_schemas)).to be true
    end

    it 'raises InvalidDataException if the schema has duplicate type' do
      expect { AvroUtils.is_valid_schema?(@invalid_duplicate_type) }.to raise_error(InvalidDataException, 
        'The name "com.example.mon_slot_def.mon_slot_def_behavior" is already in use.')
    end

    it 'raises InvalidDataException if the schema has undefined type' do
      expect { AvroUtils.is_valid_schema?(@invalid_undefined_type) }.to raise_error(InvalidDataException, 
        '"device_def" is not a schema we know about.')
    end

    it 'raises InvalidDataException if the schema has name with invalid character' do
      invalid_names = ['packagesensor-gizmo_nature/v_1.0.0: ["-", "/", ".", "."]', 'device-def: ["-"]']

      expected_error = "The following names have invalid characters:\n#{invalid_names.join("\n")}"

      expect { AvroUtils.is_valid_schema?(@invalid_character_in_name) }.to raise_error(InvalidDataException, 
        expected_error)
    end

  end

  context 'all_names_valid?' do
    before :all do
      [ :invalid_character_in_name, :referenced_schemas, :nested_schema ].each do |filename|
        eval("@#{filename} = #{File.read("./spec/fixtures/#{filename}.avsc")}")
      end
    end

    it 'returns true if all the names in data are valid' do
      [:referenced_schemas, :nested_schema ].each do |filename|
        expect(AvroUtils.all_names_valid?( eval("@#{filename}").to_json) ).to be true
      end
    end

    it 'raises InvalidDataException with detailed information if there are invalid names' do
      invalid_names = ['packagesensor-gizmo_nature/v_1.0.0: ["-", "/", ".", "."]', 'device-def: ["-"]']
      expected_error = "The following names have invalid characters:\n#{invalid_names.join("\n")}"
      expect { AvroUtils.all_names_valid?(@invalid_character_in_name.to_json) }.to raise_error(InvalidDataException, expected_error)
    end
  end

end