require 'active_support/all'
require 'json'
require 'json-schema'
require 'avro'

# avro types
# string: unicode character sequence
# bytes: sequence of 8-bit unsigned bytes
# int: 32-bit signed integer
# long: 64-bit signed integer
# float: single precision (32-bit) IEEE 754 floating-point number
# double: double precision (64-bit) IEEE 754 floating-point number
# boolean: a binary value
# null: no value

RUBY_AVRO_TYPE_MAPPING = {
  String: :string,
  Symbol: :string,
  Fixnum: :int,
  Bignum: :long,
  Float: :double,
  TrueClass: :boolean,
  FalseClass: :boolean,
  NilClass: :null

}

Dir.glob('./lib/exceptions/*.rb').sort.each { |file| puts file; require file }
Dir.glob('./lib/utils/*.rb').sort.each { |file| puts file; require file }