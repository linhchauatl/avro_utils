Gem::Specification.new do |s|
  s.name        = 'avro_utils'
  s.version     = '0.3.0'
  s.summary     = "A gem to convert different data format to avro"
  s.description = "A gem to convert different data format to avro"
  s.authors     = ['Linh Chau']
  s.email       = 'chauhonglinh@gmail.com'
  s.files       = [
                    './avro_utils.gemspec', 'lib/avro_utils.rb',
                    'lib/utils/avro_utils.rb', 'lib/exceptions/invalid_data_exception.rb'
                  ]
  s.homepage    = 'https://github.com/linhchauatl/avro_utils'
  s.license     = 'MIT'
  s.add_runtime_dependency 'json'
  s.add_runtime_dependency 'json-schema'
  s.add_runtime_dependency 'avro'
  s.add_runtime_dependency 'logging'
  s.add_runtime_dependency 'activesupport'
  s.add_development_dependency 'rspec', '~> 3.3'
end