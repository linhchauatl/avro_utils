### avro_utils

This gem provides some avro conversion utilities.
Currently is support only conversion from json to avro.


**1. AvroUtils.avro_schema((hash_data, record_type_name)**

This method takes any Ruby hash and generates an avro schema of the type Avro::Schema::RecordSchema, with the name given in record_type_name.

**2. AvroUtils.json_to_avro(json_data, record_type_name, filename = nil)**

This method converts any Ruby hash or any valid JSON string to an avro presentation of the data in the hash or json string, with a schema type of record.<br/>
The schema name is the name given in record_type_name.

If a filename is not passed, the method returns a StringIO that contains avro data.<br>
If a filename is passed, the method returns a File object, and a file with filename is created.

**3. AvroUtils.bulk_json_to_avro(collection, record_type_name, filename = nil)**

This method converts any Ruby collection of (hashes or valid JSON strings) to an avro presentation of the data in the collection, with a schema type of record.<br/>
The schema name is the name given in record_type_name.

All the elements in the collection must have the same data structure.

If a filename is not passed, the method returns a StringIO that contains avro data.<br>
If a filename is passed, the method returns a File object, and a file with filename is created.

**4. AvroUtils.is_valid_schema?(schema)**

This method validates to see if a schema is valid. If it is valid, the method returns true.<br/>
Otherwise, it raises InvalidDataException.

**5. AvroUtils.all_names_valid?(string_schema)**

This method checks if all "name" definitions in an avro schema is valid. Only [A-Za-z0-9_] are allowed.<br/>
If there is any name that contains invalid characters, it will raise InvalidDataException.
